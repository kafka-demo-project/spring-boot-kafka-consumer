package com.example.model;

import lombok.Data;

@Data
public class Message {
	private Integer msgId;
	private String sender;
	private String msg;
	private String receiveTime;
}
