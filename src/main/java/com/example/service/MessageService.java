package com.example.service;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.example.model.Message;
import com.example.repository.MessageRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MessageService {
	private static final Logger logger = LoggerFactory.getLogger(MessageService.class);

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private MessageRepository messageRepository;

	@KafkaListener(topics = "${sender.topic-name}", groupId = "${spring.kafka.consumer.group-id}", clientIdPrefix = "string", containerFactory = "kafkaListenerStringContainerFactory")
	public void listenasString(ConsumerRecord<String, String> cr, @Payload String payload) {
		Message message = null;
		try {
			// mapper payload
			message = mapper.readValue(payload, Message.class);
			message.setSender(message.getSender() + "++");

			logger.info("#{}, {}:{}, {}", message.getMsgId(), message.getSender(), message.getMsg(),
					message.getReceiveTime());

		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		// store data to mongo
		if (message != null) {
			messageRepository.insert(message);
		}
	}

}
