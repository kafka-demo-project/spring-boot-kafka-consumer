package com.example;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootKafkaComsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootKafkaComsumerApplication.class, args);
	}

}
