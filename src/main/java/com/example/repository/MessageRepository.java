package com.example.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.model.Message;

public interface MessageRepository extends MongoRepository<Message, String> {

}
